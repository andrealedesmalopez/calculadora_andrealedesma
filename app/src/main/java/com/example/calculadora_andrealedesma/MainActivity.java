package com.example.calculadora_andrealedesma;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    //números y operadores
    Button num0, num1, num2, num3, num4, num5, num6, num7, num8, num9,
            suma, resta, div, multi, raiz, igual, limpiar, punto;
    TextView vistaOp, vistaNum;
    String  operador, numerosVal;
    Double numero, numero2, resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        //Castear
        //números
        num0 = (Button) findViewById(R.id.btnCero);
        num1 = (Button) findViewById(R.id.btnUno);
        num2 = (Button) findViewById(R.id.btnDos);
        num3 = (Button) findViewById(R.id.btnTres);
        num4 = (Button) findViewById(R.id.btnCuatro);
        num5 = (Button) findViewById(R.id.btnCinco);
        num6 = (Button) findViewById(R.id.btnSeis);
        num7 = (Button) findViewById(R.id.btnSiete);
        num8 = (Button) findViewById(R.id.btnOcho);
        num9 = (Button) findViewById(R.id.btnNueve);
        //operadores y otros
        suma = (Button) findViewById(R.id.btnMas);
        resta = (Button) findViewById(R.id.btnMenos);
        div = (Button) findViewById(R.id.btnDiv);
        raiz = (Button) findViewById(R.id.btnRaiz);
        multi = (Button) findViewById(R.id.btnMulti);
        igual = (Button) findViewById(R.id.btnIgual);
        limpiar = (Button) findViewById(R.id.btnLimpiar);
        punto = (Button) findViewById(R.id.btnPunto);
        vistaOp = (TextView) findViewById(R.id.txtVista);
        vistaNum = (TextView) findViewById(R.id.txtOperacion);


        num0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(resultado==null) {//si el resultado es nulo
                    //se obtienen los números iniciales
                    numerosVal = vistaNum.getText().toString();
                    if (numerosVal.equals("0")) {
                        //si el valor inicial es cero no se añade otro
                        vistaNum.setText(numerosVal);
                    } else {//sino es cero se concatena
                        numerosVal = numerosVal + '0';
                        vistaNum.setText(numerosVal);
                    }
                }else{//si el resultado no es nulo
                    numerosVal = vistaNum.getText().toString();
                    if (numerosVal.equals("0")) {
                        vistaNum.setText(numerosVal);//no se concatena otro cero
                    } else {
                        //si no es cero se borran los valores
                        numero=null;
                        resultado=null;
                        numero2=null;
                        //se concatena el cero
                        vistaOp.setText("");
                        numerosVal = numerosVal + '0';
                        vistaNum.setText(numerosVal);
                    }
                }
            }
        });
        num1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //llamado de la función
                obtenerNum("1");
            }
        });
        num2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //llamado de la función
                obtenerNum("2");
            }
        });
        num3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //llamado de la función
                obtenerNum("3");
            }
        });
        num4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //llamado de la función
                obtenerNum("4");
            }
        });
        num5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //llamado de la función
                obtenerNum("5");

            }
        });
        num6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //llamado de la función
                obtenerNum("6");
            }
        });
        num7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //llamado de la función
                obtenerNum("7");
            }
        });
        num8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //llamado de la función
                obtenerNum("8");
            }
        });
        num9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //llamado de la función
                obtenerNum("9");
            }
        });
        punto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(resultado==null) {//si el resultado es nulo
                    numerosVal = vistaNum.getText().toString();
                    if (numerosVal.contains(".")) {//busca en el número actual un '.'
                        vistaNum.setText(numerosVal);
                    } else if (numerosVal.equals("0") || numerosVal.isEmpty()) {//si no lo encuentra y el valor inicial es 0
                        numerosVal = "0.";
                        vistaNum.setText(numerosVal);
                    } else {//si no existe '.' en la cadena de texto
                        numerosVal = numerosVal + ".";
                        vistaNum.setText(numerosVal);
                    }
                }else{//si el resultado no es nulo
                    numerosVal = vistaNum.getText().toString();
                    if (numerosVal.contains(".")) {//si el número actual contiene un'.'
                        //quita valores
                        numero=null;
                        resultado=null;
                        numero2=null;
                        vistaOp.setText("");
                        vistaNum.setText(numerosVal);
                    } else if (numerosVal.equals("0") || numerosVal.isEmpty()) {//si el número actual es 0
                        //quita valores
                        numero=null;
                        resultado=null;
                        numero2=null;
                        vistaOp.setText("");
                        numerosVal = "0.";
                        vistaNum.setText(numerosVal);
                    } else {//si no contiene '.'
                        numerosVal = numerosVal + ".";
                        vistaNum.setText(numerosVal);
                    }
                }
            }
        });
        limpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vistaNum.setText("");
                numero = null;
                resultado = null;
                numero2=null;
                vistaOp.setText("");
            }
        });
        suma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //validación de números vacíos
                if (vistaNum.getText().toString().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Ingresa un numero", Toast.LENGTH_LONG).show();
                } else {
                    //llamada de función
                    obtenerOp("+");
                }
            }
        });
        resta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //validación de números vacíos
                if (vistaNum.getText().toString().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Ingresa un numero", Toast.LENGTH_LONG).show();
                } else {
                    //llamada de función
                    obtenerOp("-");
                }
            }
        });
        div.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //validación de números vacíos
                if (vistaNum.getText().toString().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Ingresa un numero", Toast.LENGTH_LONG).show();
                } else {
                    //lamada de función
                    obtenerOp("/");
                }
            }
        });
        multi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //validación de números vacíos
                if (vistaNum.getText().toString().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Ingresa un numero", Toast.LENGTH_LONG).show();
                } else {
                    //llamada de función
                    obtenerOp("*");
                }
            }
        });
        raiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //validación de números vacíos
                if (vistaNum.getText().toString().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Ingresa un numero", Toast.LENGTH_LONG).show();
                } else {
                    //obtener la raíz del número ingresado
                    numero = Double.parseDouble(vistaNum.getText().toString());
                    vistaOp.setText("");
                    resultado = Math.sqrt(numero);
                    vistaNum.setText(String.valueOf(resultado));
                }
            }
        });
        igual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //validación de operador vacío
                if(operador==null)
                    Toast.makeText(MainActivity.this, "Ingresa un operador", Toast.LENGTH_LONG).show();
                else if(vistaNum.getText().toString().isEmpty()) {//validación de segundo número vacío
                    Toast.makeText(MainActivity.this, "Ingresa un numero", Toast.LENGTH_LONG).show();
                }else {//llamada de función
                    //se obtiene el nuevo número
                    numero2 = Double.parseDouble(vistaNum.getText().toString());
                    switch (operador) {
                        case "+"://suma
                            resultado = numero + numero2;
                            vistaOp.setText(String.valueOf(numero).concat(operador).concat(String.valueOf(numero2)));
                            vistaNum.setText(String.valueOf(resultado));
                            //se borra el número se tipo string
                            numerosVal = "";
                            //se borra el valor del número y numero2 convertido a double
                            numero = null;
                            numero2 = null;
                            //se borra el valor al operador
                            operador = "";
                            break;
                        case "-"://resta
                            resultado = numero - numero2;
                            vistaOp.setText(String.valueOf(numero).concat(operador).concat(String.valueOf(numero2)));
                            vistaNum.setText(String.valueOf(resultado));
                            //se borra el número se tipo string
                            numerosVal = "";
                            //se borra el valor del número y numero2 convertido a double
                            numero = null;
                            numero2 = null;
                            //se borra el valor al operador
                            operador = "";
                            break;
                        case "/"://división
                            if (numero2 == 0) {//no se puede dividir entre cero
                                Toast.makeText(MainActivity.this, "No es posible dividir entre cero.", Toast.LENGTH_LONG).show();
                            } else {
                                resultado = numero / numero2;
                                vistaOp.setText(String.valueOf(numero).concat(operador).concat(String.valueOf(numero2)));
                                vistaNum.setText(String.valueOf(resultado));
                                //se borra el número se tipo string
                                numerosVal = "";
                                //se borra el valor del número y numero2 convertido a double
                                numero = null;
                                numero2 = null;
                                //se borra el valor al operador
                                operador = "";
                            }
                            break;
                        case "*"://multiplicación
                            resultado = numero * numero2;
                            vistaOp.setText(String.valueOf(numero).concat(operador).concat(String.valueOf(numero2)));
                            vistaNum.setText(String.valueOf(resultado));
                            //se borra el número se tipo string
                            numerosVal = "";
                            //se borra el valor del número y numero2 convertido a double
                            numero = null;
                            numero2 = null;
                            //se borra el valor al operador
                            operador = "";
                            break;
                        default:
                            Toast.makeText(MainActivity.this, "Operación inválida", Toast.LENGTH_LONG).show();
                            break;
                    }
                }
            }
        });
    }
    public void obtenerNum(String numeroS){
        if(resultado==null) {//en caso de que no se tome el resultado de la operación anterior
            //como nevo npumero se hace la validación para obtener un número nuevo
            numerosVal = vistaNum.getText().toString();
            if (numerosVal.equals("0")) {
                //para no concatenar el 0 con el nuevo número,
                //el 0 se quita y se coloca el nuevo número
                numerosVal = numeroS;
                vistaNum.setText(numerosVal);
            } else {
                //si no hay 0 sólo se coloca el nuevo número,
                //concatenando los ya existentes
                numerosVal = numerosVal + numeroS;
                vistaNum.setText(numerosVal);
            }
        }else{
            //si el resultado no es nulo y se desea
            //trabajar con nuevos números
            if (numerosVal.equals("0")) {//si es igual a cero
                //se quitan los valores de las variables
                //para evitar errorres en la operación
                numero=null;
                resultado=null;
                numero2=null;
                //se borra la vista a la última operación
                vistaOp.setText("");
                //se asignan los nuevos números
                numerosVal = numeroS;
                vistaNum.setText(numerosVal);
            } else {//sino
                //se quitan los valores de las variables
                //para evitar errores
                numero=null;
                resultado=null;
                numero2=null;
                //se borra la última operación
                vistaOp.setText("");
                //se concatenan los números
                numerosVal = numerosVal + numeroS;
                vistaNum.setText(numerosVal);
            }
        }
    }

    public void obtenerOp(String operadorS){
        if(numero!=null) {//si el número inicial no es nulo
            operador = operadorS;
            //se obtiene el valor del nuevo operador y se concatena al número
            vistaOp.setText(String.valueOf(numero).concat(operador));
        }else if(resultado!=null){
            //si el resultado no es nulo, es decir,
            //el usuario desea trabajar con el resultado
            //de la operación anterior, a número se le asigna ese valor
            numero=resultado;
            //se borra el valor del resultado para crear uno nuevo
            resultado=null;
            operador = operadorS;
            //se concatena el número y el operador para visualizarlos
            vistaOp.setText(String.valueOf(numero).concat(operador));
            vistaNum.setText("");
        } else {//si el resultado es nullo
            //se obtiene el valor del número inicial
            numero = Double.parseDouble(vistaNum.getText().toString());
            vistaNum.setText("");
            //se obtiene el valor del operador y se concatena al número
            operador = operadorS;
            vistaOp.setText(String.valueOf(numero).concat(operador));

        }

    }
}